import networkx as nx
import numpy as np
import pandas as pd
import pytest

import phasik as pk


@pytest.fixture
def network0():
    """trivial graph, one node no edges"""
    return nx.trivial_graph()


@pytest.fixture
def network1():
    """simple graph, square + external edge"""
    nodes = ["a", "b", "c", "d", "e"]
    edges = [("a", "b"), ("a", "c"), ("b", "d"), ("c", "d"), ("a", "e")]
    G = nx.Graph()
    G.add_nodes_from(nodes)
    G.add_edges_from(edges)
    return G


@pytest.fixture
def network2():
    """simple graph (square) with isolated node e"""
    nodes = ["a", "b", "c", "d", "e"]
    edges = [("a", "b"), ("a", "c"), ("b", "d"), ("c", "d")]
    G = nx.Graph()
    G.add_nodes_from(nodes)
    G.add_edges_from(edges)
    return G


@pytest.fixture
def tedges1():
    tedges = [
        ("a", "b", 0),
        ("a", "c", 0),
        ("a", "b", 0.1),
        ("a", "b", 0.2),
        ("a", "c", 0.3),
    ]
    return tedges


@pytest.fixture
def tedges2():
    """weighted"""
    tedges = [
        ("a", "b", 0, 0.1),
        ("a", "b", 0.1, 0.5),
        ("a", "b", 0.2, 0.2),
        ("a", "c", 0, 0),
        ("a", "c", 0.1, 0.2),
        ("a", "c", 0.2, 0.4),
    ]
    return tedges


@pytest.fixture
def node_series1():
    """node times series for 3 nodes over 10 timepoints"""
    times = np.arange(0, 1, 0.1)
    nodes = ["a", "b", "c"]
    n_t = len(times)

    np.random.seed(42)  # set seed
    series = np.random.random((len(nodes), n_t)) * 2
    series_df = pd.DataFrame(series, index=nodes, columns=times)
    return series_df


@pytest.fixture
def node_series2():
    """node times series for 5 nodes over 10 timepoints"""
    times = np.arange(0, 1, 0.1)
    nodes = ["a", "b", "c", "d", "e"]
    n_t = len(times)

    np.random.seed(42)  # set seed
    series = np.random.random((len(nodes), n_t)) * 2
    series_df = pd.DataFrame(series, index=nodes, columns=times)
    return series_df


@pytest.fixture
def edge_series1():
    """edges times series for 2 edges over 10 timepoints"""
    times = np.arange(0, 1, 0.1)
    edges = ["a-b", "a-c"]
    n_t = len(times)

    np.random.seed(42)  # set seed
    series = np.random.random((len(edges), n_t)) * 2
    series_df = pd.DataFrame(series, index=edges, columns=times)
    return series_df


@pytest.fixture
def edge_series2():
    """edges times series for 3 edges (complete graph) over 10 timepoints"""
    times = np.arange(0, 1, 0.1)
    edges = ["a-b", "a-c", "b-c"]
    n_t = len(times)

    np.random.seed(42)  # set seed
    series = np.random.random((len(edges), n_t)) * 2
    series_df = pd.DataFrame(series, index=edges, columns=times)
    return series_df


@pytest.fixture
def temporal_net1(network1, node_series2):
    TG = pk.TemporalNetwork.from_static_network_and_node_timeseries(
        network1,
        node_series2,
        static_edge_default_weight=1,
        normalise="minmax",  # method to normalise the edge weights
        quiet=True,  # if True, prints less information
    )

    return TG


@pytest.fixture
def distance_matrix1(temporal_net1):
    """distance matrix built from temporal_net1"""
    distance_metric = "euclidean"
    distance_matrix = pk.DistanceMatrix.from_temporal_network(
        temporal_net1, distance_metric
    )
    return distance_matrix


@pytest.fixture
def cluster_set1(distance_matrix1):
    """cluster set built from distance_matrix1"""
    clustering_method = "ward"  # used to compute the distance between clusters
    n_max_type = (
        "maxclust"  # set number of clusters by maximum number of clusters wanted
    )
    n_max = 3  # max number of clusters
    cluster_set = pk.ClusterSet.from_distance_matrix(
        distance_matrix1, n_max_type, n_max, clustering_method
    )

    return cluster_set


@pytest.fixture
def sort_cluster_list1():
    sort_cluster_list = [10, 20, 30]
    return sort_cluster_list


@pytest.fixture
def cluster_sets1(distance_matrix1):
    """cluster sets built from distance_matrix1"""
    clustering_method = "ward"  # used to compute the distance between clusters
    n_max_type = (
        "maxclust"  # set number of clusters by maximum number of clusters wanted
    )
    n_max_range = range(2, 6)
    cluster_sets = pk.ClusterSets.from_distance_matrix(
        distance_matrix1, n_max_type, n_max_range, clustering_method
    )
    return cluster_sets


@pytest.fixture
def cluster_set2():
    """cluster set built manually"""
    clusters = [1, 1, 1, 2, 2, 3, 3, 3, 3]
    clustering_method = "ward"  # used to compute the distance between clusters
    n_max_type = (
        "maxclust"  # set number of clusters by maximum number of clusters wanted
    )
    n_max_range = range(2, 6)
    cluster_sets = pk.ClusterSets.from_distance_matrix(
        distance_matrix1, n_max_type, n_max_range, clustering_method
    )
    return cluster_sets


@pytest.fixture
def cluster_sets2(distance_matrix1):
    """cluster sets built from distance_matrix1"""
    clustering_method = "centroid"  # used to compute the distance between clusters
    n_max_type = (
        "maxclust"  # set number of clusters by maximum number of clusters wanted
    )
    n_max_range = range(2, 6)
    cluster_sets = pk.ClusterSets.from_distance_matrix(
        distance_matrix1, n_max_type, n_max_range, clustering_method
    )
    return cluster_sets


@pytest.fixture
def valid_cluster_sets1(distance_matrix1):
    clustering_methods = ["ward", "k_means", "average"]
    n_max_type = (
        "maxclust"  # set number of clusters by maximum number of clusters wanted
    )
    n_max_range = range(2, 6)
    valid_cluster_sets = []

    for clustering_method in clustering_methods:

        cluster_sets = pk.ClusterSets.from_distance_matrix(
            distance_matrix1, n_max_type, n_max_range, clustering_method
        )
        valid_cluster_sets.append((cluster_sets, clustering_method))

    return valid_cluster_sets


@pytest.fixture
def par_temp_net1(temporal_net1):
    return temporal_net1.to_partially_temporal()


@pytest.fixture
def par_temp_net2(network1, tedges2):
    return pk.PartiallyTemporalNetwork.from_static_network_and_tedges(
        network1,
        tedges2,
        static_edge_default_weight=1,
        normalise="minmax",
    )
