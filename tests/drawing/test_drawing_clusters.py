import matplotlib.pyplot as plt
import numpy as np
import pytest
from pytest import approx
from sklearn.metrics import adjusted_rand_score

import phasik as pk


def test_plot_randindex_bars_over_methods_and_sizes(valid_cluster_sets1):

    fig, ax = plt.subplots()
    ax = pk.plot_randindex_bars_over_methods_and_sizes(valid_cluster_sets1)

    assert ax.has_data()
    assert len(ax.patches) == 8
    assert [int(i.get_text()) for i in ax.get_xticklabels()] == [2, 3, 4, 5]
    assert len(set([i.get_facecolor() for i in ax.patches])) == 2

    plt.close()

    fig, ax = plt.subplots()
    ax = pk.plot_randindex_bars_over_methods_and_sizes(
        valid_cluster_sets1, plot_ref=True
    )
    leg = ax.legend()

    assert ax.has_data()
    assert len(ax.patches) == 12
    assert [int(i.get_text()) for i in ax.get_xticklabels()] == [2, 3, 4, 5]
    assert len(set([i.get_facecolor() for i in ax.patches])) == 3
    assert [i.get_text() for i in leg.get_texts()] == ["ward", "k_means", "average"]

    plt.close()


def test_plot_cluster_set(cluster_set1):

    fig, ax = plt.subplots()
    ax = pk.plot_cluster_set(cluster_set1)

    assert ax.has_data()
    colors_drawn = list(ax.collections[0].get_array().data)
    assert colors_drawn == list(cluster_set1.clusters)
    plt.close()


def test_plot_cluster_sets(cluster_sets1):

    fig, ax = plt.subplots()
    ax = pk.plot_cluster_sets(cluster_sets1, axs=ax)

    assert len(ax.collections) == 4
    for clust in cluster_sets1.clusters:
        assert adjusted_rand_score(clust, ax.get_children()[0].get_array().data)
    plt.close()

    # default ax
    ax = pk.plot_cluster_sets(cluster_sets1)

    assert len(ax.collections) == 4
    for clust in cluster_sets1.clusters:
        assert adjusted_rand_score(clust, ax.get_children()[0].get_array().data)
    plt.close()

    # errors
    with pytest.raises(AssertionError):
        ax = pk.plot_cluster_sets(cluster_sets1, with_silhouettes=True)
    with pytest.raises(AssertionError):
        ax = pk.plot_cluster_sets(cluster_sets1, with_n_clusters=True)

    # 2 axes, but second empty
    fig, (ax1, ax2) = plt.subplots(2)
    ax1 = pk.plot_cluster_sets(cluster_sets1, axs=(ax1, ax2))

    assert len(ax1.collections) == 4
    for clust in cluster_sets1.clusters:
        assert adjusted_rand_score(clust, ax1.get_children()[0].get_array().data)
    assert not ax2.has_data()
    plt.close()

    # 2 axes
    fig, (ax1, ax2) = plt.subplots(2)
    ax1, ax2 = pk.plot_cluster_sets(
        cluster_sets1, axs=(ax1, ax2), with_silhouettes=True
    )

    assert len(ax1.collections) == 4
    for clust in cluster_sets1.clusters:
        assert adjusted_rand_score(clust, ax1.get_children()[0].get_array().data)
    assert ax2.has_data()
    assert len(ax2.lines[0].get_xdata()) == 4
    plt.close()

    # 3 axes
    fig, (ax1, ax2, ax3) = plt.subplots(3)
    ax1, ax2, ax3 = pk.plot_cluster_sets(
        cluster_sets1, axs=(ax1, ax2, ax3), with_silhouettes=True, with_n_clusters=True
    )

    assert len(ax1.collections) == 4
    for clust in cluster_sets1.clusters:
        assert adjusted_rand_score(clust, ax1.get_children()[0].get_array().data)
    assert ax2.has_data()
    assert len(ax2.lines[0].get_xdata()) == 4
    assert len(ax3.lines[0].get_xdata()) == 4
    plt.close()


def test_plot_dendrogram(cluster_set1):

    ax = pk.plot_dendrogram(cluster_set1)

    assert ax.has_data()
    assert ax.lines[0].get_xdata() == [0, 1]
    # horizontal line
    assert approx(ax.lines[0].get_ydata()) == [1.8396687446574511, 1.8396687446574511]
    # four branches of dedrogram with different colors
    assert len(ax.collections) == 4

    plt.close()


def test_plot_average_silhouettes(cluster_sets1):

    fig, ax = plt.subplots()
    ax = pk.plot_average_silhouettes(cluster_sets1)
    assert ax.has_data()
    assert len(ax.lines[0].get_xdata()) == 4  # num. of num. of clusters
    plt.close()


def test_plot_ns_clusters(cluster_sets1):

    fig, ax = plt.subplots()
    ax = pk.plot_ns_clusters(cluster_sets1)
    assert ax.has_data()
    assert len(ax.lines[0].get_xdata()) == 4  # num. of num. of clusters
    plt.close()


def test_relabel_clusters_sorted():

    clusters = [2, 2, 2, 3, 3, 1, 1, 1, 3]
    clusters_relabed = pk.relabel_clusters_sorted(clusters)

    assert np.all(clusters_relabed == [1, 1, 1, 2, 2, 3, 3, 3, 2])
    assert type(clusters_relabed) == np.ndarray
    assert adjusted_rand_score(clusters, clusters_relabed) == 1

    # final labels
    final_labels = [1, 2, 4]
    clusters_relabed = pk.relabel_clusters_sorted(clusters, final_labels)
    assert np.all(clusters_relabed == [1, 1, 1, 2, 2, 4, 4, 4, 2])
    assert type(clusters_relabed) == np.ndarray
    assert adjusted_rand_score(clusters, clusters_relabed) == 1

    # final labels in different order
    final_labels = [1, 3, 2]
    clusters_relabed = pk.relabel_clusters_sorted(clusters, final_labels)
    assert np.all(clusters_relabed == [1, 1, 1, 3, 3, 2, 2, 2, 3])
    assert type(clusters_relabed) == np.ndarray
    assert adjusted_rand_score(clusters, clusters_relabed) == 1

    # final labels too many
    with pytest.raises(ValueError):
        final_labels = [1, 2, 3, 4]
        clusters_relabed = pk.relabel_clusters_sorted(clusters, final_labels)


def test_relabel_clustersets(cluster_sets1, cluster_sets2):

    out = pk.relabel_clustersets(cluster_sets1)

    exp_clusters = np.array(
        [
            [1, 2, 1, 2, 1, 1, 1, 2, 2, 1],
            [1, 2, 3, 2, 1, 1, 3, 2, 2, 3],
            [1, 4, 3, 2, 1, 1, 3, 4, 2, 3],
            [1, 4, 3, 2, 1, 1, 3, 5, 2, 3],
        ]
    )

    assert np.all(out.clusters == exp_clusters)
    assert len(out.clusters_sets) == 4
    assert out.distance_metric == "euclidean"
    assert np.all(out.n_clusters == np.array([2, 3, 4, 5]))

    for clust, clust_new in zip(out.clusters, exp_clusters):
        assert adjusted_rand_score(clust, clust_new) == 1

    # other method
    out = pk.relabel_clustersets(cluster_sets1, method="ascending")

    exp_clusters = np.array(
        [
            [1, 2, 1, 2, 1, 1, 1, 2, 2, 1],
            [1, 2, 3, 2, 1, 1, 3, 2, 2, 3],
            [1, 2, 3, 4, 1, 1, 3, 2, 4, 3],
            [1, 2, 3, 4, 1, 1, 3, 5, 4, 3],
        ]
    )

    assert np.all(out.clusters == exp_clusters)
    assert len(out.clusters_sets) == 4
    assert out.distance_metric == "euclidean"
    assert np.all(out.n_clusters == np.array([2, 3, 4, 5]))

    for clust, clust_new in zip(out.clusters, exp_clusters):
        assert adjusted_rand_score(clust, clust_new) == 1

    # translation
    out = pk.relabel_clustersets(cluster_sets1, translation={1: 2, 2: 3, 3: 1})
    exp_clusters = np.array(
        [
            [2, 3, 2, 3, 2, 2, 2, 3, 3, 2],
            [2, 3, 1, 3, 2, 2, 1, 3, 3, 1],
            [2, 4, 1, 3, 2, 2, 1, 4, 3, 1],
            [2, 4, 1, 3, 2, 2, 1, 5, 3, 1],
        ]
    )

    assert np.all(out.clusters == exp_clusters)
    assert len(out.clusters_sets) == 4
    assert out.distance_metric == "euclidean"
    assert np.all(out.n_clusters == np.array([2, 3, 4, 5]))

    for clust, clust_new in zip(out.clusters, exp_clusters):
        assert adjusted_rand_score(clust, clust_new) == 1

    # case with non-always increasing number of clusters
    out = pk.relabel_clustersets(cluster_sets2)
    exp_clusters = np.array(
        [
            [1, 1, 1, 1, 1, 1, 1, 1, 1, 1],
            [1, 1, 1, 1, 1, 1, 1, 1, 1, 1],
            [1, 3, 1, 2, 1, 1, 1, 4, 2, 1],
            [1, 3, 5, 2, 1, 1, 5, 4, 2, 5],
        ]
    )

    assert np.all(out.clusters == exp_clusters)
    assert len(out.clusters_sets) == 4
    assert out.distance_metric == "euclidean"
    assert np.all(out.n_clusters == np.array([1, 1, 4, 5]))

    for clust, clust_new in zip(out.clusters, exp_clusters):
        assert adjusted_rand_score(clust, clust_new) == 1


def test_relabel_clustersets_from_dict_valid(cluster_sets1):
    # Define a valid translation dictionary
    translation = {1: 2, 2: 3, 3: 1}

    result = pk.relabel_clustersets_from_dict(cluster_sets1, translation)
    assert isinstance(result, pk.ClusterSets)

    # Define an invalid translation dictionary
    translation = {1: 2, 2: 3, 3: 4}

    # Call the function and expect a ValueError to be raised
    with pytest.raises(ValueError):
        pk.relabel_clustersets_from_dict(cluster_sets1, translation)
