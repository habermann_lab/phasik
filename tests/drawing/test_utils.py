import matplotlib as mpl
import matplotlib.pyplot as plt
import numpy as np
import scipy.cluster.hierarchy as sch
from matplotlib import colors as mpl_colors
from matplotlib.colors import ListedColormap
from pytest import approx

import phasik as pk


def test_palette_20_ordered():
    # default
    palette = pk.palette_20_ordered()
    assert len(palette) == 20  # Check if the palette contains 20 colors
    assert isinstance(palette, list)  # Check if the palette is returned as a list

    # as map
    palette_map = pk.palette_20_ordered(as_map=True)
    assert isinstance(
        palette_map, ListedColormap
    )  # Check if the palette is returned as a ListedColormap object


def test_configure_sch_color_map():
    # Prepare dummy data and color map
    cmap = mpl.colormaps["viridis"]

    # Call the function
    pk.configure_sch_color_map(cmap)


def test_display_name():
    # known key
    assert (
        pk.display_name("maxclust") == "Max # clusters"
    )  # Check if the display name is returned correctly
    assert (
        pk.display_name("distance") == "Distance threshold"
    )  # Check if the display name is returned correctly

    # unknown key
    assert (
        pk.display_name("unknown") == "unknown"
    )  # Check if the unknown key is returned as is
    assert (
        pk.display_name("foo") == "foo"
    )  # Check if another unknown key is returned as is


def test_label_subplot_grid_with_shared_axes():
    # Prepare dummy data and subplots
    rows = 3
    columns = 4
    total_subplots = 11
    xlabel = "X"
    ylabel = "Y"

    fig, axes = plt.subplots(rows, columns, figsize=(10, 8))
    axes = np.array(axes)

    # Call the function
    pk.label_subplot_grid_with_shared_axes(axes, total_subplots, xlabel, ylabel)

    # Assertions
    # Check if the number of axes matches the total_subplots
    assert len(fig.axes) == total_subplots

    # Check if the ylabel is set for the leftmost axes in each row
    for row in range(rows):
        assert axes[row, 0].get_ylabel() == ylabel

    # Check if the xlabel is set for the bottom row of subplots
    for ax in axes[-1]:
        if ax in axes[-1, : total_subplots % columns]:
            assert ax.get_xlabel() == xlabel
        else:
            assert ax.get_xlabel() == ""

    # Check if the xlabel is set for the subplots on the extra row (if present)
    if total_subplots % columns != 0 and rows > 1:
        extra_row_axes = axes[-1, : total_subplots % columns]
        for ax in extra_row_axes:
            assert ax.get_xlabel() == xlabel

    # Check if the xlabel is set for the rightmost subplots in each row (when no extra row)
    if total_subplots % columns == 0 or rows == 1:
        rightmost_axes = axes.flatten()[-columns:]
        for ax in rightmost_axes:
            assert ax.get_xlabel() == xlabel


def test_adjust_margin():
    # default ax is None
    fig, ax = plt.subplots()
    ax.plot([1, 2, 3], [4, 5, 6])
    adjusted_ax = pk.adjust_margin(top=0.1, bottom=0.1, left=0.1, right=0.1)
    assert ax is adjusted_ax  # The function should return the modified axes object

    y_diff = 2.2
    x_diff = 2.2

    # Check if the y-limits have been adjusted correctly
    assert ax.get_ylim() == approx((3.9 - y_diff * 0.1, 6.1 + y_diff * 0.1))
    # Check if the x-limits have been adjusted correctly
    assert ax.get_xlim() == approx((0.9 - x_diff * 0.1, 3.1 + y_diff * 0.1))

    # with ax
    fig, ax = plt.subplots()
    ax.plot([1, 2, 3], [4, 5, 6])
    adjusted_ax = pk.adjust_margin(ax, top=0.2, bottom=0.3, left=0.4, right=0.5)
    assert ax is adjusted_ax  # The function should return the modified axes object

    y_diff = 2.2
    x_diff = 2.2
    # Check if the y-limits have been adjusted correctly
    assert ax.get_ylim() == approx((3.9 - y_diff * 0.3, 6.1 + y_diff * 0.2))
    # Check if the x-limits have been adjusted correctly
    assert ax.get_xlim() == approx((0.9 - x_diff * 0.4, 3.1 + x_diff * 0.5))

    # no changes
    fig, ax = plt.subplots()
    ax.plot([1, 2, 3], [4, 5, 6])
    adjusted_ax = pk.adjust_margin(ax)
    assert ax is adjusted_ax  # The function should return the unmodified axes object

    # Check if the y-limits remain unchanged
    assert ax.get_ylim() == (3.9, 6.1)
    # Check if the x-limits remain unchanged
    assert ax.get_xlim() == (0.9, 3.1)
