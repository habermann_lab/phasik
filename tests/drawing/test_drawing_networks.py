import matplotlib.pyplot as plt
import networkx as nx
from matplotlib import animation

import phasik as pk


def test_standard_params():
    # Test with different color inputs
    color1 = "red"
    params1 = pk.standard_params(color1)
    assert params1["node_color"] == color1
    assert params1["edge_color"] == color1
    assert params1["font_color"] == "k"
    assert params1["font_size"] == "medium"
    assert params1["edgecolors"] == "k"
    assert params1["node_size"] == 100
    assert params1["bbox"]["facecolor"] == color1
    assert params1["bbox"]["edgecolor"] == "black"
    assert params1["bbox"]["boxstyle"] == "round, pad=0.2"
    assert params1["bbox"]["alpha"] == 1


def test_draw_graph():
    # Create a sample graph
    graph = nx.Graph()
    graph.add_edges_from([(1, 2), (2, 3), (3, 4)])

    # Create a sample position dictionary
    pos = {1: (0, 0), 2: (1, 1), 3: (2, 2), 4: (3, 3)}

    # Create a sample axis
    fig, ax = plt.subplots()

    # Call the function
    pk.draw_graph(
        graph,
        pos=pos,
        ax=ax,
    )

    assert ax.has_data()

    plt.close()


def test_highlight_subgraphs():
    # Create sample graphs
    graph1 = nx.Graph([(1, 2), (2, 3), (3, 4)])
    graph2 = nx.Graph([(5, 6), (6, 7)])

    # Set up other parameters
    graphs = [graph1, graph2]
    colors = ["blue", "red"]
    ax = plt.gca()
    label_nodes = True
    pos = {1: (0, 0), 2: (1, 1), 3: (2, 2), 4: (3, 3), 5: (0, 4), 6: (1, 5), 7: (2, 6)}

    # Call the function
    pk.highlight_subgraphs(graphs, colors, ax, pos, label_nodes)

    assert ax.has_data()

    plt.close()


def test_animate_temporal_network():
    # Create a sample temporal network
    t_edges = [(1, 2, 0), (1, 3, 0.5), (2, 3, 0.5), (1, 3, 1)]
    temporal_network = pk.TemporalNetwork.from_tedges(t_edges)
    # Add nodes and edges to the temporal network

    # Set up other parameters
    color_temporal = "red"
    color_constant = "silver"
    width_scale = 1.5
    with_labels = True
    pos = {1: (0, 0), 2: (1, 1), 3: (2, 2)}
    ax = plt.gca()
    interval = 20
    frames = 10

    # Call the function
    ani = pk.animate_temporal_network(
        temporal_network,
        color_temporal,
        color_constant,
        width_scale,
        with_labels,
        pos,
        ax,
        interval,
        frames,
    )

    assert isinstance(ani, animation.FuncAnimation)

    plt.close()
