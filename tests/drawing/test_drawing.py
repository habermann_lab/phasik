import matplotlib.pyplot as plt
import numpy as np

import phasik as pk


def test_plot_events():

    # vertical
    fig, ax = plt.subplots()

    events = [
        (1, "Event 1", "-"),
        (2, "Event 2", "--"),
        (3, "Event 3", "-."),
    ]

    ax = pk.plot_events(events, ax=ax)

    lines = ax.lines

    assert len(lines) == len(events)
    for line, event in zip(lines, events):
        assert line.get_linestyle() == event[2]

    labels = [child for child in ax.get_children() if isinstance(child, plt.Text)]

    assert len(labels) == len(events) + 3
    plt.close()

    # horizontal
    fig, ax = plt.subplots()
    ax = pk.plot_events(events, ax=ax, orientation="horizontal")

    lines = ax.lines

    assert len(lines) == len(events)
    for line, event in zip(lines, events):
        assert line.get_linestyle() == event[2]

    labels = [child for child in ax.get_children() if isinstance(child, plt.Text)]

    assert len(labels) == len(events)
    plt.close()

    # test n_periods
    fig, ax = plt.subplots()

    period = 5
    n_periods = 3

    ax = pk.plot_events(events, ax=ax, period=period, n_periods=n_periods)

    lines = ax.lines

    assert len(lines) == len(events) * n_periods
    for line, event in zip(lines, events * n_periods):
        assert line.get_linestyle() == event[2]

    labels = [child for child in ax.get_children() if isinstance(child, plt.Text)]

    assert len(labels) == len(events) * n_periods + 3
    plt.close()

    # no labels
    fig, ax = plt.subplots()
    ax = pk.plot_events(events, ax=ax, add_labels=False)
    lines = ax.lines

    assert len(lines) == len(events)
    for line, event in zip(lines, events):
        assert line.get_linestyle() == event[2]

    labels = [child for child in ax.get_children() if isinstance(child, plt.Text)]

    assert len(labels) == 0 + 3
    plt.close()


def test_plot_phases():
    # Prepare dummy data and parameters
    phases = [(0, 2, "Phase 1"), (3, 5, "Phase 2"), (6, 8, "Phase 3")]
    fig, ax = plt.subplots()
    y_pos = 1.01
    ymin = 0
    ymax = 1
    t_offset = 0

    # Call the function
    ax = pk.plot_phases(
        phases, ax=ax, y_pos=y_pos, ymin=ymin, ymax=ymax, t_offset=t_offset
    )

    # Assertions
    assert len(ax.patches) == len(
        phases
    )  # Check if the correct number of patches are created

    for i, phase in enumerate(phases):
        start_time, end_time, name = phase
        start_time += t_offset
        end_time += t_offset
        mid_time = (start_time + end_time) / 2
        alpha_interval = 0.5 / len(phases) * (i + 1)
        patch = ax.patches[i]
        assert (
            patch.get_xy()[0][0] == start_time
        )  # Check if the x-coordinate of the patch is correct

        # Check if the text is correctly positioned and has the correct content
        text = ax.texts[i]
        assert (
            text.get_position()[0] == mid_time
        )  # Check if the x-coordinate of the text is correct
        assert (
            text.get_position()[1] == y_pos
        )  # Check if the y-coordinate of the text is correct
        assert text.get_text() == name  # Check if the text content is correct
        assert (
            text.get_fontweight() == "bold"
        )  # Check if the text font weight is set to 'bold'
        assert (
            text.get_verticalalignment() == "center"
        )  # Check if the text vertical alignment is set to 'center'
        assert (
            text.get_horizontalalignment() == "center"
        )  # Check if the text horizontal alignment is set to 'center'

    plt.close()


def test_plot_interval():
    binary_series = np.array([0, 1, 1, 0, 0, 1, 1, 1, 0])
    times = np.array([1, 2, 3, 4, 5, 6, 7, 8, 9])
    y = 0
    color = "k"
    fig, ax = plt.subplots()

    ax = pk.plot_interval(binary_series, times, y=y, color=color, ax=ax)

    plt.close()
