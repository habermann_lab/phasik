from collections.abc import Sequence

import matplotlib.pyplot as plt
import numpy as np
import pytest
from pytest import approx
from sklearn.metrics import adjusted_rand_score

import phasik as pk
from phasik.classes.clustering import ClusterSet
from phasik.drawing.drawing import plot_events, plot_phases
from phasik.drawing.drawing_clusters import plot_cluster_sets, relabel_clustersets
from phasik.drawing.utils import adjust_margin, display_name


def test_plot(cluster_sets1):

    fig, ax = plt.subplots()
    ax = cluster_sets1.plot(axs=ax)

    assert len(ax.collections) == 4
    for clust in cluster_sets1.clusters:
        assert adjusted_rand_score(clust, ax.get_children()[0].get_array().data)
    plt.close()

    # default ax
    ax = cluster_sets1.plot()
    assert len(ax.collections) == 4
    for clust in cluster_sets1.clusters:
        assert adjusted_rand_score(clust, ax.get_children()[0].get_array().data)
    plt.close()

    # errors
    with pytest.raises(AssertionError):
        ax = cluster_sets1.plot(with_silhouettes=True)
    with pytest.raises(AssertionError):
        ax = cluster_sets1.plot(with_n_clusters=True)

    # 2 axes, but second empty
    fig, (ax1, ax2) = plt.subplots(2)
    ax1 = cluster_sets1.plot(axs=(ax1, ax2))

    assert len(ax1.collections) == 4
    for clust in cluster_sets1.clusters:
        assert adjusted_rand_score(clust, ax1.get_children()[0].get_array().data)
    assert not ax2.has_data()
    plt.close()

    # 2 axes
    fig, (ax1, ax2) = plt.subplots(2)
    ax1, ax2 = cluster_sets1.plot(axs=(ax1, ax2), with_silhouettes=True)

    assert len(ax1.collections) == 4
    for clust in cluster_sets1.clusters:
        assert adjusted_rand_score(clust, ax1.get_children()[0].get_array().data)
    assert ax2.has_data()
    assert len(ax2.lines[0].get_xdata()) == 4
    plt.close()

    # 3 axes
    fig, (ax1, ax2, ax3) = plt.subplots(3)
    ax1, ax2, ax3 = cluster_sets1.plot(
        axs=(ax1, ax2, ax3), with_silhouettes=True, with_n_clusters=True
    )

    assert len(ax1.collections) == 4
    for clust in cluster_sets1.clusters:
        assert adjusted_rand_score(clust, ax1.get_children()[0].get_array().data)
    assert ax2.has_data()
    assert len(ax2.lines[0].get_xdata()) == 4
    assert len(ax3.lines[0].get_xdata()) == 4
    plt.close()


def test_plot_and_format_with_average_silhouettes(cluster_sets1):

    fig, axs = plt.subplots(3)

    # creating events for test
    events = [
        (1, "Event 1", "-"),
        (2, "Event 2", "--"),
        (3, "Event 3", "-."),
    ]

    # creating phases for test
    phases = [(0, 2, "Phase 1"), (3, 5, "Phase 2"), (6, 8, "Phase 3")]

    cluster_sets1.plot_and_format_with_average_silhouettes(axs, events, phases)
    ax1, ax2, ax3 = axs[0], axs[1], axs[2]

    # Assertions for phases
    assert len(ax1.patches) == len(
        phases
    )  # Check if the correct number of patches are created

    for i, phase in enumerate(phases):
        start_time, end_time, name = phase
        start_time += 0
        end_time += 0
        mid_time = (start_time + end_time) / 2
        alpha_interval = 0.5 / len(phases) * (i + 1)
        patch = ax1.patches[i]
        assert (
            patch.get_xy()[0][0] == start_time
        )  # Check if the x-coordinate of the patch is correct

        # Check if the text is correctly positioned and has the correct content
        text = ax1.texts[i + 3]  # add + 3 because in three events are plotted before.
        assert (
            text.get_position()[0] == mid_time
        )  # Check if the x-coordinate of the text is correct

        assert text.get_text() == name  # Check if the text content is correct

        assert (
            text.get_fontweight() == "bold"
        )  # Check if the text font weight is set to 'bold'
        assert (
            text.get_verticalalignment() == "center"
        )  # Check if the text vertical alignment is set to 'center'
        assert (
            text.get_horizontalalignment() == "center"
        )  # Check if the text horizontal alignment is set to 'center'

    # test events
    lines = ax1.lines

    assert len(lines) == len(events)
    for line, event in zip(lines, events):
        assert line.get_linestyle() == event[2]

    labels = [child for child in ax1.get_children() if isinstance(child, plt.Text)]

    assert (
        len(labels) == len(events) + 6
    )  # adding + 6 because of the phases, plotted in the same ax1

    assert len(ax1.collections) == 4
    for clust in cluster_sets1.clusters:
        assert adjusted_rand_score(clust, ax1.get_children()[0].get_array().data)
    assert ax2.has_data()
    assert len(ax2.lines[0].get_xdata()) == 4
    assert len(ax3.lines[0].get_xdata()) == 4
    plt.close()


def test_plot_silhouette_samples(cluster_sets1):

    fig, axs = plt.subplots(len(cluster_sets1.clusters))
    cluster_sets1.plot_silhouette_samples(axs=axs)

    for cluster_set, ax in zip(cluster_sets1.n_clusters, axs.flatten()):
        assert ax.has_data()
        assert len(ax.collections) == cluster_set
    plt.close()
