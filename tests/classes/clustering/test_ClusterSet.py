import matplotlib.pyplot as plt
import numpy as np
import pytest
from pytest import approx
from sklearn.metrics import adjusted_rand_score

import phasik as pk


def test_plot_dendrogram(cluster_set1):

    fig, ax = plt.subplots()
    ax = cluster_set1.plot_dendrogram()

    assert ax.has_data()
    assert ax.lines[0].get_xdata() == [0, 1]
    # horizontal line
    assert approx(ax.lines[0].get_ydata()) == [1.8396687446574511, 1.8396687446574511]
    # four branches of dedrogram with different colors
    assert len(ax.collections) == 4
    plt.close()


def test_plot(cluster_set1):

    fig, ax = plt.subplots()
    ax = cluster_set1.plot()

    assert ax.has_data()
    colors_drawn = list(ax.collections[0].get_array().data)
    assert colors_drawn == list(cluster_set1.clusters)
    plt.close()


def test_plot_silhouette_samples(cluster_set1):

    fig, ax = plt.subplots()
    cluster_set1.plot_silhouette_samples()

    assert ax.has_data()
    assert len(ax.lines[0].get_xdata()) == 2
    plt.close()
