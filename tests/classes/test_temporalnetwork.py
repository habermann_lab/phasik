import networkx as nx
import numpy as np
import pandas as pd
import pytest

import phasik as pk


def test_temporalnetwork_from_static_network_and_node_timeseries(
    network1, node_series2
):

    G = network1

    N = G.number_of_nodes()
    nodes = list(G.nodes)

    node_series = node_series2
    times = list(node_series2.columns)
    T = len(times)  # number of timepoints

    TG = pk.TemporalNetwork.from_static_network_and_node_timeseries(
        G,
        node_series,
        static_edge_default_weight=1,
        normalise="minmax",  # method to normalise the edge weights
        quiet=False,  # if True, prints less information
    )

    assert isinstance(TG, pk.TemporalNetwork)

    assert TG.nodes == nodes
    assert TG.times == times

    tedges_df = TG.tedges
    assert list(tedges_df.columns) == ["i", "j", "t", "weight"]

    snaps = TG.snapshots
    assert snaps.shape == (T, N, N)

    assert len(TG) == N
    assert nodes == [n for n in TG]

    assert "a" in TG
    assert "z" not in TG

    assert TG.N() == N
    assert TG.T() == T
    assert TG.shape() == (N, T)

    assert TG.number_of_edges() == 5
    assert TG.is_weighted() is True

    assert TG.has_node("a") is True
    assert TG.has_node("z") is False

    assert TG.has_time(0.1) is True
    assert TG.has_time(-1) is False

    assert list(TG.neighbors().keys()) == nodes
    assert TG.edges_aggregated() == list(G.edges)

    series_ab = TG.edge_timeseries(edges=[("a", "b")])
    edge_ab = node_series.loc["a"] * node_series.loc["b"]
    edge_ab_ground = edge_ab - np.min(edge_ab)
    assert np.all(series_ab["a-b"] == edge_ab_ground / np.max(edge_ab_ground))

    # normalization
    for e, series in TG.edge_timeseries().items():
        assert np.max(series) == 1
        assert np.min(series) == 0

    TG_max = pk.TemporalNetwork.from_static_network_and_node_timeseries(
        G,
        node_series,
        static_edge_default_weight=1,
        normalise="max",  # method to normalise the edge weights
        quiet=False,  # if True, prints less information
    )

    # test __str__ method
    description = TG.__str__()
    assert isinstance(description, str)

    # test __contains__ on a non existing node
    assert "fake_node" not in TG

    # test has_tedge_method
    # tedge of lengh 3
    tedges_serie = TG.has_tedge([1, 2, 3])
    # tedge of lengh 4
    TG.has_tedge([1, 2, 3, 4])
    # invalid tedge
    with pytest.raises(Exception):
        TG.has_tedge([1])

    assert isinstance(tedges_serie, pd.Series)

    # test tedges_of_edge
    with pytest.raises(ValueError):
        TG.tedges_of_edge(("fn1", "fn2"))

    masked_tedges, mask = TG.tedges_of_edge(("a", "b"), return_mask=True, reverse=True)
    assert isinstance(masked_tedges, pd.DataFrame)
    assert isinstance(mask, pd.Series)

    masked_tedges = TG.tedges_of_edge(("a", "b"), return_mask=False, reverse=True)
    assert isinstance(masked_tedges, pd.DataFrame)

    # test tedges_of_node
    with pytest.raises(ValueError):
        TG.tedges_of_node("fn1")

    masked_tedges, mask = TG.tedges_of_node("a", return_mask=True, reverse=True)
    assert isinstance(masked_tedges, pd.DataFrame)
    assert isinstance(mask, pd.Series)

    masked_tedges = TG.tedges_of_node("a", return_mask=False, reverse=True)
    assert isinstance(masked_tedges, pd.DataFrame)


def test_temporalnetwork_from_static_network_and_node_timeseries_partial(
    network1, node_series1, capfd
):

    G = network1

    N = G.number_of_nodes()
    nodes = list(G.nodes)

    node_series = node_series1
    times = list(node_series1.columns)
    T = len(times)  # number of timepoints

    TG = pk.TemporalNetwork.from_static_network_and_node_timeseries(
        G,
        node_series,
        static_edge_default_weight=1,
        normalise="minmax",  # method to normalise the edge weights
        quiet=False,  # if True, prints less information
    )

    captured = capfd.readouterr()
    missing = {("a", "e"), ("b", "d"), ("c", "d")}
    output = f"WARNING: 3/5 edges in the static network have no temporal information. \nA PartiallyTemporalNetwork is created instead.\nEdges with no temporal information:"
    assert output in captured.out.strip()
    assert isinstance(TG, pk.PartiallyTemporalNetwork)

    TG = pk.TemporalNetwork.from_static_network_and_node_timeseries(
        G,
        node_series,
        static_edge_default_weight=1,
        normalise="minmax",  # method to normalise the edge weights
        quiet=True,  # if True, prints less information
    )

    captured = capfd.readouterr()
    output = f"WARNING: 3/5 edges in the static network have no temporal information. \nA PartiallyTemporalNetwork is created instead."
    assert captured.out.strip() == output
    assert isinstance(TG, pk.PartiallyTemporalNetwork)

    assert TG.nodes == nodes
    assert TG.times == times

    tedges_df = TG.tedges
    assert list(tedges_df.columns) == ["i", "j", "t", "weight"]

    snaps = TG.snapshots
    assert snaps.shape == (T, N, N)

    assert len(TG) == N
    assert nodes == [n for n in TG]

    assert "a" in TG
    assert "z" not in TG

    assert TG.N() == N
    assert TG.T() == T
    assert TG.shape() == (N, T)

    assert TG.number_of_edges() == 5
    assert TG.is_weighted() is True

    assert TG.has_node("a") is True
    assert TG.has_node("z") is False

    assert TG.has_time(0.1) is True
    assert TG.has_time(-1) is False

    assert list(TG.neighbors().keys()) == nodes
    assert TG.edges_aggregated() == list(G.edges)

    series_ab = TG.edge_timeseries(edges=[("a", "b")])
    edge_ab = node_series.loc["a"] * node_series.loc["b"]
    edge_ab_ground = edge_ab - np.min(edge_ab)
    assert np.all(series_ab["a-b"] == edge_ab_ground / np.max(edge_ab_ground))

    # normalization
    for e, series in TG.edge_timeseries().items():
        assert np.max(series) == 1
        assert np.min(series) == 0


def test_temporalnetwork_from_node_timeseries(node_series2):

    node_series = node_series2

    nodes = list(node_series.index)
    times = list(node_series.columns)

    T = len(times)  # number of timepoints
    N = len(nodes)

    TG = pk.TemporalNetwork.from_node_timeseries(node_series, normalise="minmax")

    assert isinstance(TG, pk.TemporalNetwork)
    assert TG.nodes == nodes
    assert TG.times == times

    tedges_df = TG.tedges
    assert list(tedges_df.columns) == ["i", "j", "t", "weight"]

    snaps = TG.snapshots
    assert snaps.shape == (T, N, N)

    assert len(TG) == N
    assert nodes == [n for n in TG]

    assert "a" in TG
    assert "z" not in TG

    assert TG.N() == N
    assert TG.T() == T
    assert TG.shape() == (N, T)

    assert TG.number_of_edges() == N * (N - 1) / 2  # complete graph
    assert TG.is_weighted() is True

    assert TG.has_node("a") is True
    assert TG.has_node("z") is False

    assert TG.has_time(0.1) is True
    assert TG.has_time(-1) is False

    assert list(TG.neighbors().keys()) == nodes

    series_ab = TG.edge_timeseries(edges=[("a", "b")])
    edge_ab = node_series.loc["a"] * node_series.loc["b"]
    edge_ab_ground = edge_ab - np.min(edge_ab)
    assert np.all(series_ab["a-b"] == edge_ab_ground / np.max(edge_ab_ground))

    # normalization
    for e, series in TG.edge_timeseries().items():
        assert np.max(series) == 1
        assert np.min(series) == 0


def test_temporalnetwork_from_edge_timeseries(edge_series2):

    edge_series = edge_series2

    nodes = ["a", "b", "c"]
    times = list(edge_series.columns)

    T = len(times)  # number of timepoints
    N = len(nodes)

    TG = pk.TemporalNetwork.from_edge_timeseries(edge_series, normalise="minmax")

    assert isinstance(TG, pk.TemporalNetwork)
    assert TG.nodes == nodes
    assert TG.times == times

    tedges_df = TG.tedges
    assert list(tedges_df.columns) == ["i", "j", "t", "weight"]

    snaps = TG.snapshots
    assert snaps.shape == (T, N, N)

    assert len(TG) == N
    assert nodes == [n for n in TG]

    assert "a" in TG
    assert "z" not in TG

    assert TG.N() == N
    assert TG.T() == T
    assert TG.shape() == (N, T)

    assert TG.number_of_edges() == N * (N - 1) / 2  # complete graph
    assert TG.is_weighted() is True

    assert TG.has_node("a") is True
    assert TG.has_node("z") is False

    assert TG.has_time(0.1) is True
    assert TG.has_time(-1) is False

    assert list(TG.neighbors().keys()) == nodes

    series_ab = TG.edge_timeseries(edges=[("a", "b")])
    edge_ab = edge_series.loc["a-b"]
    edge_ab_ground = edge_ab - np.min(edge_ab)
    assert np.all(series_ab["a-b"] == edge_ab_ground / np.max(edge_ab_ground))

    # normalization
    for e, series in TG.edge_timeseries().items():
        assert np.max(series) == 1
        assert np.min(series) == 0


def test_nodes(temporal_net1):
    assert temporal_net1.nodes == ["a", "b", "c", "d", "e"]


def test_tedges(temporal_net1):

    tedg = {
        "i": [
            "a",
            "a",
            "a",
            "a",
            "a",
            "a",
            "a",
            "a",
            "a",
            "a",
            "a",
            "a",
            "a",
            "a",
            "a",
            "a",
            "a",
            "a",
            "a",
            "a",
            "a",
            "a",
            "a",
            "a",
            "a",
            "a",
            "a",
            "a",
            "a",
            "a",
            "b",
            "b",
            "b",
            "b",
            "b",
            "b",
            "b",
            "b",
            "b",
            "b",
            "c",
            "c",
            "c",
            "c",
            "c",
            "c",
            "c",
            "c",
            "c",
            "c",
        ],
        "j": [
            "b",
            "b",
            "b",
            "b",
            "b",
            "b",
            "b",
            "b",
            "b",
            "b",
            "c",
            "c",
            "c",
            "c",
            "c",
            "c",
            "c",
            "c",
            "c",
            "c",
            "e",
            "e",
            "e",
            "e",
            "e",
            "e",
            "e",
            "e",
            "e",
            "e",
            "d",
            "d",
            "d",
            "d",
            "d",
            "d",
            "d",
            "d",
            "d",
            "d",
            "d",
            "d",
            "d",
            "d",
            "d",
            "d",
            "d",
            "d",
            "d",
            "d",
        ],
        "t": [
            0.0,
            0.1,
            0.2,
            0.3,
            0.4,
            0.5,
            0.6,
            0.7,
            0.8,
            0.9,
            0.0,
            0.1,
            0.2,
            0.3,
            0.4,
            0.5,
            0.6,
            0.7,
            0.8,
            0.9,
            0.0,
            0.1,
            0.2,
            0.3,
            0.4,
            0.5,
            0.6,
            0.7,
            0.8,
            0.9,
            0.0,
            0.1,
            0.2,
            0.3,
            0.4,
            0.5,
            0.6,
            0.7,
            0.8,
            0.9,
            0.0,
            0.1,
            0.2,
            0.3,
            0.4,
            0.5,
            0.6,
            0.7,
            0.8,
            0.9,
        ],
        "weight": [
            0.0,
            1.0,
            0.657955956426974,
            0.1305875150301791,
            0.022592325869345894,
            0.022856996728861396,
            0.010894353946222571,
            0.48865160636842475,
            0.2755245162441753,
            0.2170846477568075,
            0.5015116107093074,
            0.27896611971805446,
            0.46620814597747345,
            0.478834385127092,
            0.1372865884162834,
            0.2556023617358028,
            0.0,
            1.0,
            0.794135013372164,
            0.04908141009223532,
            0.05245038383161051,
            0.8601459119450116,
            0.013428322419706148,
            1.0,
            0.04231538888141729,
            0.161979621833756,
            0.0,
            0.821569949767855,
            0.5900624786102131,
            0.21431146347807467,
            0.0,
            0.5401508607092308,
            0.14713458941899757,
            0.6676658421723292,
            0.5761274141710464,
            0.47963290228025773,
            0.28324245406193205,
            0.13689717350272215,
            1.0,
            0.4086954754420455,
            0.5728546262094624,
            0.007767359044118861,
            0.0,
            0.5337260569863472,
            0.6843771187321938,
            1.0,
            0.06791764191799836,
            0.050707137273118624,
            0.6274590047647318,
            0.002339971983602632,
        ],
    }

    ted = pd.DataFrame(tedg)
    assert all(ted.loc[:, "i"] == temporal_net1.tedges.loc[:, "i"])
    assert all(ted.loc[:, "j"] == temporal_net1.tedges.loc[:, "j"])
    assert all(
        np.round(np.array(list(ted.loc[:, "t"])), decimals=3)
        == np.round(np.array(list(temporal_net1.tedges.loc[:, "t"])), decimals=3)
    )
    assert all(
        np.round(ted.loc[:, "weight"], decimals=3)
        == np.round(temporal_net1.tedges.loc[:, "weight"], decimals=3)
    )


def test_snapshots(temporal_net1):

    snap_true = np.array(
        [
            [
                [0.0, 0.0, 0.50151161, 0.0, 0.05245038],
                [0.0, 0.0, 0.0, 0.0, 0.0],
                [0.50151161, 0.0, 0.0, 0.57285463, 0.0],
                [0.0, 0.0, 0.57285463, 0.0, 0.0],
                [0.05245038, 0.0, 0.0, 0.0, 0.0],
            ],
            [
                [0.0, 1.0, 0.27896612, 0.0, 0.86014591],
                [1.0, 0.0, 0.0, 0.54015086, 0.0],
                [0.27896612, 0.0, 0.0, 0.00776736, 0.0],
                [0.0, 0.54015086, 0.00776736, 0.0, 0.0],
                [0.86014591, 0.0, 0.0, 0.0, 0.0],
            ],
            [
                [0.0, 0.65795596, 0.46620815, 0.0, 0.01342832],
                [0.65795596, 0.0, 0.0, 0.14713459, 0.0],
                [0.46620815, 0.0, 0.0, 0.0, 0.0],
                [0.0, 0.14713459, 0.0, 0.0, 0.0],
                [0.01342832, 0.0, 0.0, 0.0, 0.0],
            ],
            [
                [0.0, 0.13058752, 0.47883439, 0.0, 1.0],
                [0.13058752, 0.0, 0.0, 0.66766584, 0.0],
                [0.47883439, 0.0, 0.0, 0.53372606, 0.0],
                [0.0, 0.66766584, 0.53372606, 0.0, 0.0],
                [1.0, 0.0, 0.0, 0.0, 0.0],
            ],
            [
                [0.0, 0.02259233, 0.13728659, 0.0, 0.04231539],
                [0.02259233, 0.0, 0.0, 0.57612741, 0.0],
                [0.13728659, 0.0, 0.0, 0.68437712, 0.0],
                [0.0, 0.57612741, 0.68437712, 0.0, 0.0],
                [0.04231539, 0.0, 0.0, 0.0, 0.0],
            ],
            [
                [0.0, 0.022857, 0.25560236, 0.0, 0.16197962],
                [0.022857, 0.0, 0.0, 0.4796329, 0.0],
                [0.25560236, 0.0, 0.0, 1.0, 0.0],
                [0.0, 0.4796329, 1.0, 0.0, 0.0],
                [0.16197962, 0.0, 0.0, 0.0, 0.0],
            ],
            [
                [0.0, 0.01089435, 0.0, 0.0, 0.0],
                [0.01089435, 0.0, 0.0, 0.28324245, 0.0],
                [0.0, 0.0, 0.0, 0.06791764, 0.0],
                [0.0, 0.28324245, 0.06791764, 0.0, 0.0],
                [0.0, 0.0, 0.0, 0.0, 0.0],
            ],
            [
                [0.0, 0.48865161, 1.0, 0.0, 0.82156995],
                [0.48865161, 0.0, 0.0, 0.13689717, 0.0],
                [1.0, 0.0, 0.0, 0.05070714, 0.0],
                [0.0, 0.13689717, 0.05070714, 0.0, 0.0],
                [0.82156995, 0.0, 0.0, 0.0, 0.0],
            ],
            [
                [0.0, 0.27552452, 0.79413501, 0.0, 0.59006248],
                [0.27552452, 0.0, 0.0, 1.0, 0.0],
                [0.79413501, 0.0, 0.0, 0.627459, 0.0],
                [0.0, 1.0, 0.627459, 0.0, 0.0],
                [0.59006248, 0.0, 0.0, 0.0, 0.0],
            ],
            [
                [0.0, 0.21708465, 0.04908141, 0.0, 0.21431146],
                [0.21708465, 0.0, 0.0, 0.40869548, 0.0],
                [0.04908141, 0.0, 0.0, 0.00233997, 0.0],
                [0.0, 0.40869548, 0.00233997, 0.0, 0.0],
                [0.21431146, 0.0, 0.0, 0.0, 0.0],
            ],
        ]
    )

    assert all(
        np.round(temporal_net1.snapshots.flatten(), decimals=3)
        == np.round(snap_true.flatten(), decimals=3)
    )


def test_N(temporal_net1):
    assert 5 == temporal_net1.N()


def test_T(temporal_net1):
    assert 10 == temporal_net1.T()


def test_shape(temporal_net1):

    assert 5, 10 == temporal_net1.shape


def test_number_of_edges(temporal_net1):

    assert 5 == temporal_net1.number_of_edges()


def test_is_weighted(temporal_net1):
    assert temporal_net1.is_weighted


def test_has_node(temporal_net1):
    assert temporal_net1.has_node("a")


def test_has_time(temporal_net1):
    assert temporal_net1.has_time(0.1)
