import numpy as np
import pandas as pd
import pytest

import phasik as pk


def test_partiallytemporalnetwork_from_static_network_and_tedges(
    network1, tedges1, tedges2
):

    G = network1  # 5 nodes
    tedges = tedges1  # info about 3 nodes

    N = G.number_of_nodes()

    nodes = list(G.nodes)
    times = [0.0, 0.1, 0.2, 0.3]

    TG = pk.PartiallyTemporalNetwork.from_static_network_and_tedges(
        static_network=G,
        tedges=tedges,
    )


def test_nodes(par_temp_net1, par_temp_net2):
    assert par_temp_net1.nodes == ["a", "b", "c", "d", "e"]
    assert par_temp_net2.nodes == ["a", "b", "c", "d", "e"]


def test_tedges(par_temp_net1, par_temp_net2):

    # testing on par_temp_net1
    tedg = {
        "i": [
            "a",
            "a",
            "a",
            "a",
            "a",
            "a",
            "a",
            "a",
            "a",
            "a",
            "a",
            "a",
            "a",
            "a",
            "a",
            "a",
            "a",
            "a",
            "a",
            "a",
            "a",
            "a",
            "a",
            "a",
            "a",
            "a",
            "a",
            "a",
            "a",
            "a",
            "b",
            "b",
            "b",
            "b",
            "b",
            "b",
            "b",
            "b",
            "b",
            "b",
            "c",
            "c",
            "c",
            "c",
            "c",
            "c",
            "c",
            "c",
            "c",
            "c",
        ],
        "j": [
            "b",
            "b",
            "b",
            "b",
            "b",
            "b",
            "b",
            "b",
            "b",
            "b",
            "c",
            "c",
            "c",
            "c",
            "c",
            "c",
            "c",
            "c",
            "c",
            "c",
            "e",
            "e",
            "e",
            "e",
            "e",
            "e",
            "e",
            "e",
            "e",
            "e",
            "d",
            "d",
            "d",
            "d",
            "d",
            "d",
            "d",
            "d",
            "d",
            "d",
            "d",
            "d",
            "d",
            "d",
            "d",
            "d",
            "d",
            "d",
            "d",
            "d",
        ],
        "t": [
            0.0,
            0.1,
            0.2,
            0.3,
            0.4,
            0.5,
            0.6,
            0.7,
            0.8,
            0.9,
            0.0,
            0.1,
            0.2,
            0.3,
            0.4,
            0.5,
            0.6,
            0.7,
            0.8,
            0.9,
            0.0,
            0.1,
            0.2,
            0.3,
            0.4,
            0.5,
            0.6,
            0.7,
            0.8,
            0.9,
            0.0,
            0.1,
            0.2,
            0.3,
            0.4,
            0.5,
            0.6,
            0.7,
            0.8,
            0.9,
            0.0,
            0.1,
            0.2,
            0.3,
            0.4,
            0.5,
            0.6,
            0.7,
            0.8,
            0.9,
        ],
        "weight": [
            0.0,
            1.0,
            0.657955956426974,
            0.1305875150301791,
            0.022592325869345894,
            0.022856996728861396,
            0.010894353946222571,
            0.48865160636842475,
            0.2755245162441753,
            0.2170846477568075,
            0.5015116107093074,
            0.27896611971805446,
            0.46620814597747345,
            0.478834385127092,
            0.1372865884162834,
            0.2556023617358028,
            0.0,
            1.0,
            0.794135013372164,
            0.04908141009223532,
            0.05245038383161051,
            0.8601459119450116,
            0.013428322419706148,
            1.0,
            0.04231538888141729,
            0.161979621833756,
            0.0,
            0.821569949767855,
            0.5900624786102131,
            0.21431146347807467,
            0.0,
            0.5401508607092308,
            0.14713458941899757,
            0.6676658421723292,
            0.5761274141710464,
            0.47963290228025773,
            0.28324245406193205,
            0.13689717350272215,
            1.0,
            0.4086954754420455,
            0.5728546262094624,
            0.007767359044118861,
            0.0,
            0.5337260569863472,
            0.6843771187321938,
            1.0,
            0.06791764191799836,
            0.050707137273118624,
            0.6274590047647318,
            0.002339971983602632,
        ],
    }

    ted = pd.DataFrame(tedg)
    assert all(ted.loc[:, "i"] == par_temp_net1.tedges.loc[:, "i"])
    assert all(ted.loc[:, "j"] == par_temp_net1.tedges.loc[:, "j"])
    assert all(
        np.round(np.array(list(ted.loc[:, "t"])), decimals=3)
        == np.round(np.array(list(par_temp_net1.tedges.loc[:, "t"])), decimals=3)
    )
    assert all(
        np.round(ted.loc[:, "weight"], decimals=3)
        == np.round(par_temp_net1.tedges.loc[:, "weight"], decimals=3)
    )

    # testing on par_temp_net2
    data = {
        "i": [
            "a",
            "a",
            "a",
            "a",
            "a",
            "a",
            "a",
            "a",
            "a",
            "b",
            "b",
            "b",
            "c",
            "c",
            "c",
        ],
        "j": [
            "b",
            "b",
            "b",
            "c",
            "c",
            "c",
            "e",
            "e",
            "e",
            "d",
            "d",
            "d",
            "d",
            "d",
            "d",
        ],
        "t": [
            0.0,
            0.1,
            0.2,
            0.0,
            0.1,
            0.2,
            0.0,
            0.1,
            0.2,
            0.0,
            0.1,
            0.2,
            0.0,
            0.1,
            0.2,
        ],
        "weight": [
            0.0,
            1.0,
            0.25,
            0.0,
            0.5,
            1.0,
            0.5,
            0.5,
            0.5,
            0.5,
            0.5,
            0.5,
            0.5,
            0.5,
            0.5,
        ],
    }

    tedge_true = pd.DataFrame(data)

    assert list(tedge_true.loc[:, "i"]) == list(par_temp_net2.tedges.loc[:, "i"])
    assert list(tedge_true.loc[:, "j"]) == list(par_temp_net2.tedges.loc[:, "j"])
    assert list(tedge_true.loc[:, "t"]) == list(par_temp_net2.tedges.loc[:, "t"])
    assert list(tedge_true.loc[:, "weight"]) == list(
        par_temp_net2.tedges.loc[:, "weight"]
    )


def test_snapshots(par_temp_net1, par_temp_net2):

    true_snap1 = np.array(
        [
            [
                [0.0, 0.0, 0.50151161, 0.0, 0.05245038],
                [0.0, 0.0, 0.0, 0.0, 0.0],
                [0.50151161, 0.0, 0.0, 0.57285463, 0.0],
                [0.0, 0.0, 0.57285463, 0.0, 0.0],
                [0.05245038, 0.0, 0.0, 0.0, 0.0],
            ],
            [
                [0.0, 1.0, 0.27896612, 0.0, 0.86014591],
                [1.0, 0.0, 0.0, 0.54015086, 0.0],
                [0.27896612, 0.0, 0.0, 0.00776736, 0.0],
                [0.0, 0.54015086, 0.00776736, 0.0, 0.0],
                [0.86014591, 0.0, 0.0, 0.0, 0.0],
            ],
            [
                [0.0, 0.65795596, 0.46620815, 0.0, 0.01342832],
                [0.65795596, 0.0, 0.0, 0.14713459, 0.0],
                [0.46620815, 0.0, 0.0, 0.0, 0.0],
                [0.0, 0.14713459, 0.0, 0.0, 0.0],
                [0.01342832, 0.0, 0.0, 0.0, 0.0],
            ],
            [
                [0.0, 0.13058752, 0.47883439, 0.0, 1.0],
                [0.13058752, 0.0, 0.0, 0.66766584, 0.0],
                [0.47883439, 0.0, 0.0, 0.53372606, 0.0],
                [0.0, 0.66766584, 0.53372606, 0.0, 0.0],
                [1.0, 0.0, 0.0, 0.0, 0.0],
            ],
            [
                [0.0, 0.02259233, 0.13728659, 0.0, 0.04231539],
                [0.02259233, 0.0, 0.0, 0.57612741, 0.0],
                [0.13728659, 0.0, 0.0, 0.68437712, 0.0],
                [0.0, 0.57612741, 0.68437712, 0.0, 0.0],
                [0.04231539, 0.0, 0.0, 0.0, 0.0],
            ],
            [
                [0.0, 0.022857, 0.25560236, 0.0, 0.16197962],
                [0.022857, 0.0, 0.0, 0.4796329, 0.0],
                [0.25560236, 0.0, 0.0, 1.0, 0.0],
                [0.0, 0.4796329, 1.0, 0.0, 0.0],
                [0.16197962, 0.0, 0.0, 0.0, 0.0],
            ],
            [
                [0.0, 0.01089435, 0.0, 0.0, 0.0],
                [0.01089435, 0.0, 0.0, 0.28324245, 0.0],
                [0.0, 0.0, 0.0, 0.06791764, 0.0],
                [0.0, 0.28324245, 0.06791764, 0.0, 0.0],
                [0.0, 0.0, 0.0, 0.0, 0.0],
            ],
            [
                [0.0, 0.48865161, 1.0, 0.0, 0.82156995],
                [0.48865161, 0.0, 0.0, 0.13689717, 0.0],
                [1.0, 0.0, 0.0, 0.05070714, 0.0],
                [0.0, 0.13689717, 0.05070714, 0.0, 0.0],
                [0.82156995, 0.0, 0.0, 0.0, 0.0],
            ],
            [
                [0.0, 0.27552452, 0.79413501, 0.0, 0.59006248],
                [0.27552452, 0.0, 0.0, 1.0, 0.0],
                [0.79413501, 0.0, 0.0, 0.627459, 0.0],
                [0.0, 1.0, 0.627459, 0.0, 0.0],
                [0.59006248, 0.0, 0.0, 0.0, 0.0],
            ],
            [
                [0.0, 0.21708465, 0.04908141, 0.0, 0.21431146],
                [0.21708465, 0.0, 0.0, 0.40869548, 0.0],
                [0.04908141, 0.0, 0.0, 0.00233997, 0.0],
                [0.0, 0.40869548, 0.00233997, 0.0, 0.0],
                [0.21431146, 0.0, 0.0, 0.0, 0.0],
            ],
        ]
    )

    true_snap2 = np.array(
        [
            0.0,
            0.0,
            0.0,
            0.0,
            0.5,
            0.0,
            0.0,
            0.0,
            0.5,
            0.0,
            0.0,
            0.0,
            0.0,
            0.5,
            0.0,
            0.0,
            0.5,
            0.5,
            0.0,
            0.0,
            0.5,
            0.0,
            0.0,
            0.0,
            0.0,
            0.0,
            1.0,
            0.5,
            0.0,
            0.5,
            1.0,
            0.0,
            0.0,
            0.5,
            0.0,
            0.5,
            0.0,
            0.0,
            0.5,
            0.0,
            0.0,
            0.5,
            0.5,
            0.0,
            0.0,
            0.5,
            0.0,
            0.0,
            0.0,
            0.0,
            0.0,
            0.25,
            1.0,
            0.0,
            0.5,
            0.25,
            0.0,
            0.0,
            0.5,
            0.0,
            1.0,
            0.0,
            0.0,
            0.5,
            0.0,
            0.0,
            0.5,
            0.5,
            0.0,
            0.0,
            0.5,
            0.0,
            0.0,
            0.0,
            0.0,
        ]
    )

    assert all(true_snap2 == par_temp_net2.snapshots.flatten())
    assert all(
        np.round(par_temp_net1.snapshots.flatten(), decimals=3)
        == np.round(true_snap1.flatten(), decimals=3)
    )


def test_temporal_nodes(par_temp_net1, par_temp_net2):
    assert ["a", "b", "c", "d", "e"] == par_temp_net1.temporal_nodes
    assert set(["b", "a", "c"]) == set(par_temp_net2.temporal_nodes)


def test_temporal_edges(par_temp_net1, par_temp_net2):
    assert [
        ("a", "b"),
        ("a", "c"),
        ("a", "e"),
        ("b", "d"),
        ("c", "d"),
    ] == par_temp_net1.temporal_edges
    assert list([("a", "b"), ("a", "c")]) == list(par_temp_net2.temporal_edges)


def test_temporal_neighbors(par_temp_net1, par_temp_net2):

    true_out1 = {
        "a": ["b", "c", "e"],
        "b": ["a", "d"],
        "c": ["a", "d"],
        "d": ["b", "c"],
        "e": ["a"],
    }
    true_out2 = {"a": ["b", "c"], "b": ["a"], "c": ["a"], "d": ["b", "c"], "e": ["a"]}
    assert true_out1 == par_temp_net1.temporal_neighbors()
    assert true_out2 == par_temp_net2.temporal_neighbors()


def test_number_of_temporal_nodes(par_temp_net1, par_temp_net2):
    assert 5 == par_temp_net1.number_of_temporal_nodes()
    assert 3 == par_temp_net2.number_of_temporal_nodes()


def test_number_of_temporal_edges(par_temp_net1, par_temp_net2):
    assert 5 == par_temp_net1.number_of_temporal_edges()
    assert 2 == par_temp_net2.number_of_temporal_edges()


def test_fraction_of_temporal_nodes(par_temp_net1, par_temp_net2):
    assert 1 == par_temp_net1.fraction_of_temporal_nodes()
    assert 3 / 5 == par_temp_net2.fraction_of_temporal_nodes()


def test_fraction_of_temporal_edges(par_temp_net1, par_temp_net2):
    assert 1 == par_temp_net1.fraction_of_temporal_edges()
    assert 2 / 5 == par_temp_net2.fraction_of_temporal_edges()


def test_from_static_network_and_tedges(network1, tedges2):

    partial_temp_net = pk.PartiallyTemporalNetwork.from_static_network_and_tedges(
        network1,
        tedges2,
        static_edge_default_weight=1,
        normalise="minmax",
    )

    assert ["a", "b", "c", "d", "e"] == partial_temp_net.nodes

    data = {
        "i": [
            "a",
            "a",
            "a",
            "a",
            "a",
            "a",
            "a",
            "a",
            "a",
            "b",
            "b",
            "b",
            "c",
            "c",
            "c",
        ],
        "j": [
            "b",
            "b",
            "b",
            "c",
            "c",
            "c",
            "e",
            "e",
            "e",
            "d",
            "d",
            "d",
            "d",
            "d",
            "d",
        ],
        "t": [
            0.0,
            0.1,
            0.2,
            0.0,
            0.1,
            0.2,
            0.0,
            0.1,
            0.2,
            0.0,
            0.1,
            0.2,
            0.0,
            0.1,
            0.2,
        ],
        "weight": [
            0.0,
            1.0,
            0.25,
            0.0,
            0.5,
            1.0,
            0.5,
            0.5,
            0.5,
            0.5,
            0.5,
            0.5,
            0.5,
            0.5,
            0.5,
        ],
    }

    tedge_true = pd.DataFrame(data)

    assert list(tedge_true.loc[:, "i"]) == list(partial_temp_net.tedges.loc[:, "i"])
    assert list(tedge_true.loc[:, "j"]) == list(partial_temp_net.tedges.loc[:, "j"])
    assert list(tedge_true.loc[:, "t"]) == list(partial_temp_net.tedges.loc[:, "t"])
    assert list(tedge_true.loc[:, "weight"]) == list(
        partial_temp_net.tedges.loc[:, "weight"]
    )

    true_snap = np.array(
        [
            0.0,
            0.0,
            0.0,
            0.0,
            0.5,
            0.0,
            0.0,
            0.0,
            0.5,
            0.0,
            0.0,
            0.0,
            0.0,
            0.5,
            0.0,
            0.0,
            0.5,
            0.5,
            0.0,
            0.0,
            0.5,
            0.0,
            0.0,
            0.0,
            0.0,
            0.0,
            1.0,
            0.5,
            0.0,
            0.5,
            1.0,
            0.0,
            0.0,
            0.5,
            0.0,
            0.5,
            0.0,
            0.0,
            0.5,
            0.0,
            0.0,
            0.5,
            0.5,
            0.0,
            0.0,
            0.5,
            0.0,
            0.0,
            0.0,
            0.0,
            0.0,
            0.25,
            1.0,
            0.0,
            0.5,
            0.25,
            0.0,
            0.0,
            0.5,
            0.0,
            1.0,
            0.0,
            0.0,
            0.5,
            0.0,
            0.0,
            0.5,
            0.5,
            0.0,
            0.0,
            0.5,
            0.0,
            0.0,
            0.0,
            0.0,
        ]
    )

    assert all(true_snap == partial_temp_net.snapshots.flatten())
    assert list([("a", "b"), ("a", "c")]) == list(partial_temp_net.temporal_edges)
    assert set(["a", "b", "c"]) == set(partial_temp_net.temporal_nodes)
    assert {
        "a": ["b", "c"],
        "b": ["a"],
        "c": ["a"],
        "d": ["b", "c"],
        "e": ["a"],
    } == partial_temp_net.temporal_neighbors()
    assert 2 == partial_temp_net.number_of_temporal_edges()
    assert 3 == partial_temp_net.number_of_temporal_nodes()
    assert 2 / 5 == partial_temp_net.fraction_of_temporal_edges()
    assert 3 / 5 == partial_temp_net.fraction_of_temporal_nodes()
