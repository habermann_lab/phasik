import sys

import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
import pytest

import phasik as pk
from phasik.classes.TemporalData import TemporalData, normed


def test_temporal_data_from_df():
    lst = [[1, 2, 3], [4, 5, 6], [7, 8, 9], [10, 11, 12], [13, 14, 15]]
    edge_series = pd.DataFrame(lst, columns=["A", "B", "C"]).transpose()
    times = list(edge_series.columns)
    temp_data = TemporalData.from_df(
        edge_series.transpose(), times=times, true_times=times, i_tend=4
    )

    # testing save_to_csv()
    temp_data.save_to_csv("temp_data")
    data_true = temp_data.to_df().transpose()
    data_to_test = pd.read_csv("temp_data")
    pd.testing.assert_frame_equal(data_to_test, data_true)

    assert temp_data.temporal_data.tolist() == lst[:4]
    assert temp_data.variables == ["A", "B", "C"]
    assert temp_data.times == [0, 1, 2, 3]
    assert temp_data.true_times == [0, 1, 2, 3]

    for v in temp_data.variables:
        temp_series = temp_data.series(v)
        assert list(temp_series) == list(edge_series.loc[v, :3])

    temp_dict = temp_data.to_dict()
    expect_dict = {}

    for v in temp_data.variables:
        temp_series = temp_data.series(v)
        expect_dict[v] = temp_data.series(v)
    assert str(temp_dict) == str(expect_dict)

    temp_data2 = TemporalData.from_dict(
        temp_dict, temp_data.times, temp_data.true_times, i_tend=3
    )
    l2 = temp_data2.temporal_data.tolist()
    l = temp_data.temporal_data.tolist()
    assert temp_data2.temporal_data.tolist() == temp_data.temporal_data.tolist()[:-1]

    assert str(edge_series.iloc[:, :-1]) == str(temp_data.to_df())

    assert str(temp_data.downsample(2).temporal_data.tolist()) == str(
        [[1, 2, 3], [7, 8, 9]]
    )

    assert temp_data.normalise().temporal_data.tolist() == [
        [0.1, 0.18181818181818182, 0.25],
        [0.4, 0.45454545454545453, 0.5],
        [0.7, 0.7272727272727273, 0.75],
        [1.0, 1.0, 1.0],
    ]

    o, ot = temp_data.relative_optima("A", "maxima")
    assert tuple(ot[0]) == ()
    assert list(o) == []

    assert normed([1, 2]).tolist() == [0.5, 1.0]


def test_plot_functions():

    lst = [[1, 2, 3], [4, 5, 6], [7, 8, 9], [10, 11, 12], [13, 14, 15]]
    edge_series = pd.DataFrame(lst, columns=["A", "B", "C"]).transpose()
    times = list(edge_series.columns)
    temp_data = TemporalData.from_df(
        edge_series.transpose(), times=times, true_times=times, i_tend=4
    )

    # here we test the default version
    temp_data.plot_series()

    assert plt.gcf() is not None
    ax = plt.gca()

    assert ax.has_data()
    assert len(ax.collections) == 0
    lines = ax.lines
    for li, var in zip(lines, ["A", "B", "C"]):
        assert li.get_linestyle() == "-"
        assert all(li.get_ydata() == temp_data.series(var))

    plt.close()

    # here we gave the ax in input and also norm = True
    fig, ax = plt.subplots()
    temp_data.plot_series(ax=ax, norm=True)

    assert ax.has_data()
    assert len(ax.collections) == 0
    lines = ax.lines
    for li, var in zip(lines, ["A", "B", "C"]):
        assert li.get_linestyle() == "-"
        assert all(li.get_ydata() == normed(temp_data.series(var)))

    plt.close()
