import networkx as nx
import numpy as np
import pandas as pd

import phasik as pk


def test_graph_size_info():

    # create a random network of 15 nodes and 20 edges
    input_graph = nx.dense_gnm_random_graph(15, 20)
    out1 = "15 nodes and 20 edges"
    out = pk.graph_size_info(input_graph)

    assert out == out1


def test_weighted_edges_as_df():

    # Create a networkx.Graph, with 4 nodes and some edges
    network_input = nx.Graph()
    network_input.add_nodes_from([1, 2, 3, 4])
    network_input.add_edge(1, 2, weight=3)
    network_input.add_edge(1, 3, weight=5)
    network_input.add_edge(2, 3, weight=1)
    network_input.add_edge(2, 4, weight=4)

    # Expected output
    out1 = pd.DataFrame({"i": [1, 2, 1, 2], "j": [3, 4, 2, 3], "weight": [5, 4, 3, 1]})

    # Call the function
    out = pk.weighted_edges_as_df(network_input)

    pd.testing.assert_frame_equal(out1, out)
