import networkx
import numpy as np
import pytest

import phasik as pk


def test_aggregate_network_by_cluster(temporal_net1, cluster_set1, sort_cluster_list1):
    # Define the input parameters for the function
    temporal_network = temporal_net1
    clusters = cluster_set1.clusters
    sort_clusters = False
    output = "averaged"

    # Call the function to get the result
    result = pk.aggregate_network_by_cluster(
        temporal_network, clusters=clusters, sort_clusters=sort_clusters, output=output
    )
    # Perform assertions to validate the result
    assert isinstance(result, dict)  # Check if the result is a dictionary

    # Check if each cluster label in the result is a key in the dictionary
    for cluster_label in set(clusters):
        assert cluster_label in result

    # Check if each value in the result is a tuple with two elements
    for value in result.values():
        assert isinstance(value, tuple)
        assert len(value) == 2

        graph, time_indices = value
        assert isinstance(graph, networkx.Graph)  # Assuming you're using networkx
        assert isinstance(time_indices, list)

    # Call the function with sort_cluster True
    result = pk.aggregate_network_by_cluster(
        temporal_network, clusters, sort_clusters=True, output=output
    )
    assert isinstance(result, dict)  # Check if the result is a dictionary

    # Call the function with sort_cluster as a list
    n_cluster = len(set(clusters))
    sort_clusters = sort_cluster_list1
    result = pk.aggregate_network_by_cluster(
        temporal_network, clusters, sort_clusters=sort_clusters, output=output
    )
    assert isinstance(result, dict)  # Check if the result is a dictionary

    # Call the function with sort_cluster not valid
    with pytest.raises(ValueError):
        sort_clusters = "a"
        result = pk.aggregate_network_by_cluster(
            temporal_network, clusters, sort_clusters=sort_clusters, output=output
        )


def test_convert_cluster_labels_to_dict():

    clusters_1 = np.array([3, 3, 1, 1, 2, 1])
    time_dict_1 = {3: [0, 1], 1: [2, 3, 5], 2: [4]}

    time_dict = pk.convert_cluster_labels_to_dict(clusters_1)

    assert time_dict == time_dict_1


def test_cluster_sort():

    clusters = np.array([2, 2, 2, 3, 3, 1, 1, 1])
    out = np.array([1, 1, 1, 2, 2, 3, 3, 3])
    ordered = pk.cluster_sort(clusters)
    assert np.all(ordered == out)
