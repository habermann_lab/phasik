import phasik as pk


def test_slugify():
    # Test case 1: Basic text
    text_in = "Hello_World!"
    expected_out = "Hello_World"

    assert pk.slugify(text_in) == expected_out

    # Test case 2: Text with non-alphanumeric characters
    text_in = "Hello$@ World!"
    expected_out = "Hello"
    assert pk.slugify(text_in) == expected_out

    # Test case 3: Text with custom keep_characters
    text_in = "Hello#World!"
    keep_characters = ["#", "!"]
    expected_out = "Hello#World!"
    assert pk.slugify(text_in, keep_characters) == expected_out

    # Test case 4: Empty text
    text_in = ""
    expected_out = ""
    assert pk.slugify(text_in) == expected_out

    # Test case 5: Text with only keep_characters
    text_in = "#@!"
    keep_characters = ["#", "!"]
    expected_out = "#"
    assert pk.slugify(text_in, keep_characters) == expected_out
