# Contributing

When contributing to this repository, please first discuss the change you wish to make via an [issue](../../issues/new). Feature additions, bug fixes, etc. should all be addressed with a pull request (PR).


## Pull Request process

1. Download the dependencies in the developer [requirements file](/requirements/developer.txt).
2. [Optional] Label commits according to [Conventional Commits](https://www.conventionalcommits.org) style.
3. [Optional, but strongly preferred] Add unit tests for features being added or bugs being fixed.
4. [Optional, but strongly preferred] Include any new method/function in the corresponding docs file.
5. Run `pytest` to verify all unit tests pass.
6. [Optional, but strongly preferred] Run `isort .` to sort any new import statements.
7. [Optional, but STRONGLY preferred] Run `black .` for consistent styling.
8. Submit Pull Request with a list of changes, links to issues that it addresses (if applicable)
9. You may merge the Pull Request in once you have the sign-off of at least one other developer, or if you do not have permission to do that, you may request the reviewer to merge it for you.

## Attribution

This Contributing Statement is adapted from [this template by @PurpleBooth](https://gist.github.com/PurpleBooth/b24679402957c63ec426).