﻿phasik.classes.TemporalNetwork.TemporalNetwork.from\_static\_network\_and\_node\_timeseries
===========================================================================================

.. currentmodule:: phasik.classes.TemporalNetwork

.. automethod:: TemporalNetwork.from_static_network_and_node_timeseries