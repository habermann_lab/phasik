﻿phasik.classes.PartiallyTemporalNetwork.PartiallyTemporalNetwork.number\_of\_temporal\_edges
============================================================================================

.. currentmodule:: phasik.classes.PartiallyTemporalNetwork

.. automethod:: PartiallyTemporalNetwork.number_of_temporal_edges