﻿phasik.classes.TemporalNetwork.TemporalNetwork
==============================================

.. currentmodule:: phasik.classes.TemporalNetwork

.. autoclass:: TemporalNetwork

   
   .. automethod:: __init__

   
   .. rubric:: Methods

   .. autosummary::
   
      ~TemporalNetwork.N
      ~TemporalNetwork.T
      ~TemporalNetwork.__init__
      ~TemporalNetwork.add_tedges
      ~TemporalNetwork.aggregated_network
      ~TemporalNetwork.discard_temporal_info_from_edge
      ~TemporalNetwork.discard_temporal_info_from_node
      ~TemporalNetwork.edge_timeseries
      ~TemporalNetwork.edges_aggregated
      ~TemporalNetwork.from_edge_timeseries
      ~TemporalNetwork.from_node_timeseries
      ~TemporalNetwork.from_static_network_and_edge_timeseries
      ~TemporalNetwork.from_static_network_and_node_timeseries
      ~TemporalNetwork.from_static_network_and_tedges
      ~TemporalNetwork.from_tedges
      ~TemporalNetwork.has_node
      ~TemporalNetwork.has_tedge
      ~TemporalNetwork.has_time
      ~TemporalNetwork.is_weighted
      ~TemporalNetwork.neighbors
      ~TemporalNetwork.network_at_time
      ~TemporalNetwork.number_of_edges
      ~TemporalNetwork.shape
      ~TemporalNetwork.tedges_of_edge
      ~TemporalNetwork.tedges_of_node
      ~TemporalNetwork.to_partially_temporal
   
   

   
   
   .. rubric:: Attributes

   .. autosummary::
   
      ~TemporalNetwork.nodes
      ~TemporalNetwork.snapshots
      ~TemporalNetwork.tedges
      ~TemporalNetwork.times
   
   