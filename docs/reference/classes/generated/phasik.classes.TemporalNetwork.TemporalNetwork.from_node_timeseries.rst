﻿phasik.classes.TemporalNetwork.TemporalNetwork.from\_node\_timeseries
=====================================================================

.. currentmodule:: phasik.classes.TemporalNetwork

.. automethod:: TemporalNetwork.from_node_timeseries