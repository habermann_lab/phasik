﻿phasik.classes.PartiallyTemporalNetwork.PartiallyTemporalNetwork.fraction\_of\_temporal\_nodes
==============================================================================================

.. currentmodule:: phasik.classes.PartiallyTemporalNetwork

.. automethod:: PartiallyTemporalNetwork.fraction_of_temporal_nodes