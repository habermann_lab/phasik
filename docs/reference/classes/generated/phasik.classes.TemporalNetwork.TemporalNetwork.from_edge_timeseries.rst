﻿phasik.classes.TemporalNetwork.TemporalNetwork.from\_edge\_timeseries
=====================================================================

.. currentmodule:: phasik.classes.TemporalNetwork

.. automethod:: TemporalNetwork.from_edge_timeseries