.. _distancematrix:

DistanceMatrix
===============

.. currentmodule:: phasik.classes.DistanceMatrix
.. autoclass:: DistanceMatrix
    :members:
    
