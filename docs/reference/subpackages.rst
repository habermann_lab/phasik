Subpackages
=============

Phasik is divided into 3 subpackages: 

- **classes**: contains the :ref:`TemporalNetwork<temporalnetwork>` and :ref:`PartiallyTemporalNetwork<partiallytemporalnetwork>` classes and other classes for phase inference by clustering, such as :ref:`DistanceMatrix<distancematrix>`, :ref:`ClusterSet<clusterset>`, :ref:`ClusterSets<clustersets>`. The Phasik workflow is based on these classes: first, build a :class:`TemporalNetwork` from which to build a :class:`DistanceMatrix`, from which to then build a :class:`ClusterSet` or :class:`ClusterSets`. 

- **drawing**: contains useful visualisation functions for (temporal) networks and clusters

- **utils**: contains useful functions for networks and clusters 
