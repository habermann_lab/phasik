

<!DOCTYPE html>
<html class="writer-html5" lang="en" >
<head>
  <meta charset="utf-8">
  
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  
  <title>Theoretical background &mdash; phasik  documentation</title>
  

  
  <link rel="stylesheet" href="_static/css/theme.css" type="text/css" />
  <link rel="stylesheet" href="_static/pygments.css" type="text/css" />
  <link rel="stylesheet" href="_static/pygments.css" type="text/css" />
  <link rel="stylesheet" href="_static/css/theme.css" type="text/css" />
  <link rel="stylesheet" href="_static/graphviz.css" type="text/css" />
  <link rel="stylesheet" href="_static/copybutton.css" type="text/css" />

  
  
  
  

  
  <!--[if lt IE 9]>
    <script src="_static/js/html5shiv.min.js"></script>
  <![endif]-->
  
    
      <script type="text/javascript" id="documentation_options" data-url_root="./" src="_static/documentation_options.js"></script>
        <script data-url_root="./" id="documentation_options" src="_static/documentation_options.js"></script>
        <script src="_static/jquery.js"></script>
        <script src="_static/underscore.js"></script>
        <script src="_static/doctools.js"></script>
        <script src="_static/clipboard.min.js"></script>
        <script src="_static/copybutton.js"></script>
        <script async="async" src="https://cdn.jsdelivr.net/npm/mathjax@3/es5/tex-mml-chtml.js"></script>
    
    <script type="text/javascript" src="_static/js/theme.js"></script>

    
    <link rel="author" title="About these documents" href="about.html" />
    <link rel="index" title="Index" href="genindex.html" />
    <link rel="search" title="Search" href="search.html" />
    <link rel="next" title="Reference" href="reference/index.html" />
    <link rel="prev" title="Getting started" href="about.html" /> 
</head>

<body class="wy-body-for-nav">

   
  <div class="wy-grid-for-nav">
    
    <nav data-toggle="wy-nav-shift" class="wy-nav-side">
      <div class="wy-side-scroll">
        <div class="wy-side-nav-search" >
          

          
            <a href="index.html" class="icon icon-home" alt="Documentation Home"> phasik
          

          
            
            <img src="_static/phasik_logo_small.svg" class="logo" alt="Logo"/>
          
          </a>

          
            
            
              <div class="version">
                1.0.0
              </div>
            
          

          
<div role="search">
  <form id="rtd-search-form" class="wy-form" action="search.html" method="get">
    <input type="text" name="q" placeholder="Search docs" />
    <input type="hidden" name="check_keywords" value="yes" />
    <input type="hidden" name="area" value="default" />
  </form>
</div>

          
        </div>

        
        <div class="wy-menu wy-menu-vertical" data-spy="affix" role="navigation" aria-label="main navigation">
          
            
            
              
            
            
              <p class="caption" role="heading"><span class="caption-text">Contents:</span></p>
<ul class="current">
<li class="toctree-l1"><a class="reference internal" href="about.html">Getting started</a></li>
<li class="toctree-l1 current"><a class="current reference internal" href="#">Theoretical background</a><ul>
<li class="toctree-l2"><a class="reference internal" href="#temporal-networks">Temporal networks</a></li>
<li class="toctree-l2"><a class="reference internal" href="#inferring-the-multiscale-temporal-organisation-of-temporal-networks">Inferring the multiscale temporal organisation of temporal networks</a></li>
</ul>
</li>
<li class="toctree-l1"><a class="reference internal" href="reference/index.html">Reference</a></li>
<li class="toctree-l1"><a class="reference internal" href="tutorial/index.html">Tutorial</a></li>
<li class="toctree-l1"><a class="reference internal" href="contributors.html">Contributors</a></li>
<li class="toctree-l1"><a class="reference internal" href="contact.html">Contact</a></li>
</ul>

            
          
        </div>
        
      </div>
    </nav>

    <section data-toggle="wy-nav-shift" class="wy-nav-content-wrap">

      
      <nav class="wy-nav-top" aria-label="top navigation">
        
          <i data-toggle="wy-nav-top" class="fa fa-bars"></i>
          <a href="index.html">phasik</a>
        
      </nav>


      <div class="wy-nav-content">
        
        <div class="rst-content">
        
          















<div role="navigation" aria-label="breadcrumbs navigation">

  <ul class="wy-breadcrumbs">
    
      <li><a href="index.html" class="icon icon-home"></a> &raquo;</li>
        
      <li>Theoretical background</li>
    
    
      <li class="wy-breadcrumbs-aside">
        
            
            <a href="_sources/theory.rst.txt" rel="nofollow"> View page source</a>
          
        
      </li>
    
  </ul>

  
  <hr/>
</div>
          <div role="main" class="document" itemscope="itemscope" itemtype="http://schema.org/Article">
           <div itemprop="articleBody">
            
  <div class="section" id="theoretical-background">
<span id="theory"></span><h1>Theoretical background<a class="headerlink" href="#theoretical-background" title="Permalink to this headline">¶</a></h1>
<div class="section" id="temporal-networks">
<h2>Temporal networks<a class="headerlink" href="#temporal-networks" title="Permalink to this headline">¶</a></h2>
<p>Phasik works with <em>temporal networks</em>, sometimes called <em>dynamic networks</em> or <em>time-varying networks</em>. Temporal networks are networks with time-varying edges. They are a natural representation of systems that contain many interacting units (nodes), whose interactions change over time. Temporal networks are often used to model social contact networks, or brain networks, for example.</p>
<p>Temporal networks are a generalisation of the (static) networks in which there is no time dimension: they are composed of nodes and (constant) edges. In temporal networks, however, time is resolved and edges can vary in time. Hence, whereas static networks can be described by adjacency matrix <span class="math notranslate nohighlight">\(A_{ij}\)</span>, the adjacency matrix to describe temporal networks needs to be time-varying <span class="math notranslate nohighlight">\(A_{ij}(t)\)</span>. Phasik implements temporal networks as a dedicated class <code class="xref py py-class docutils literal notranslate"><span class="pre">TemporalNetwork</span></code>.</p>
<p>Phasik provides functions to build a TemporalNetwork by combining a static network to temporal data in several forms:</p>
<ul class="simple">
<li><p>time series of nodes: e.g., time series of protein concentrations</p></li>
<li><p>time series of edges: e.g., time series relative to protein-protein interactions</p></li>
<li><p>a list of <em>t-edges</em>, sometimes called timestamped interactions, of the form (i,j,t,weight), where i and j are two nodes</p></li>
<li><p>a list of adjacency matrices (or snapshots), representating the time-evolution of the adjacency matrix</p></li>
</ul>
<p>A particularity of Phasik is that it can build <em>partially</em> temporal networks, i.e. for which we have temporal information about only part of the edges. This is useful in many situations when using experimental data from systems in which the recording of all variables is not always possible. Phasik has a dedicated class <code class="xref py py-class docutils literal notranslate"><span class="pre">PartiallyTemporalNetwork</span></code> for this too.</p>
<p>Phasik also provides several functions to visualise temporal networks, either as static images or as animations.</p>
<p>For more details, see <a class="reference internal" href="reference/index.html#reference"><span class="std std-ref">Reference</span></a> and <a class="reference internal" href="tutorial/index.html#tutorial"><span class="std std-ref">Tutorial</span></a>.</p>
<p>Further reading: Holme, P., &amp; Saramäki, J. (2012). Temporal networks. Phys. Rep., <a class="reference external" href="https://doi.org/10.1016/j.physrep.2012.03.001">519(3)</a>, 97-125.</p>
</div>
<div class="section" id="inferring-the-multiscale-temporal-organisation-of-temporal-networks">
<h2>Inferring the multiscale temporal organisation of temporal networks<a class="headerlink" href="#inferring-the-multiscale-temporal-organisation-of-temporal-networks" title="Permalink to this headline">¶</a></h2>
<p>Systems often go through various phases, or states, over time. A good example of this is the cell cycle, which is typically divided into 4 main phases and multiple subphases. This multiscale temporal organisation in phases of a system can yield insight into its functioning but also its fate. That is why inferring and predicting theses phases can further our understanding of these systems.</p>
<p>The function and dynamics of a network is often linked to its structure, or topology. Hence, if snapshots of the temporal network at 2 different times are very similar, they can be grouped into a same <em>phase</em>. On the contrary, if two snapshots are very different, the temporal network can be said in two different phases. This idea can be formalised by clustering snapshots of a given temporal network. Since snapshots have a 1:1 correspondence to timepoints, clusters of snapshots have a can be related to time intervals. These clusters can then be linked back to those edges most active at those times to interpret them from a microscopic standpoint.</p>
<p>Further reading:</p>
<ul class="simple">
<li><p>Masuda, N., &amp; Holme, P. (2019). <a class="reference external" href="https://doi.org/10.1038/s41598-018-37534-2">Detecting sequences of system states in temporal networks</a>. Sci. Rep., 9(1) , 1-11.</p></li>
<li><p>Lucas, M., Morris, A., Townsend-Teague, A., Tichit, L., Habermann, B. H., &amp; Barrat, A. (2021). <a class="reference external" href="https://doi.org/10.1101/2021.03.26.437187">Inferring cell cycle phases from a partially temporal network of protein interactions</a>. bioRxiv.</p></li>
</ul>
</div>
</div>


           </div>
           
          </div>
          <footer>
  
    <div class="rst-footer-buttons" role="navigation" aria-label="footer navigation">
      
        <a href="reference/index.html" class="btn btn-neutral float-right" title="Reference" accesskey="n" rel="next">Next <span class="fa fa-arrow-circle-right"></span></a>
      
      
        <a href="about.html" class="btn btn-neutral float-left" title="Getting started" accesskey="p" rel="prev"><span class="fa fa-arrow-circle-left"></span> Previous</a>
      
    </div>
  

  <hr/>

  <div role="contentinfo">
    <p>
        
        &copy; Copyright 2021, Maxime Lucas, Alex Townsend-Teague, Arthur Morris
      <span class="lastupdated">
        Last updated on Jul 27, 2021.
      </span>

    </p>
  </div>
    
    
    
    Built with <a href="http://sphinx-doc.org/">Sphinx</a> using a
    
    <a href="https://github.com/rtfd/sphinx_rtd_theme">theme</a>
    
    provided by <a href="https://readthedocs.org">Read the Docs</a>. 

</footer>

        </div>
      </div>

    </section>

  </div>
  

  <script type="text/javascript">
      jQuery(function () {
          SphinxRtdTheme.Navigation.enable(true);
      });
  </script>

  
  
    
   

</body>
</html>