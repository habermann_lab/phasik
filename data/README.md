* `chen`: ODE models of S. cerevisiae cell cycle
	* accessible at: http://mpf.biol.vt.edu/research/budding_yeast_model/pp/getwinpp_current_model.php
	* described in https://doi.org/10.1091/mbc.e03-11-0794
* `kegg`: PATHWAY Database
	* S. cerevisiae static cell cycle network
		* KEGG: sce04111
		* https://doi.org/10.1093/nar/28.1.27

* `kelliher`: temporal RNA-sequencing of S. cerevisiae cell cycle
	* GEO: GSE80474
	* https://doi.org/10.1371/journal.pgen.1006453

* `mouse`: network and time series data
	* M. musculus static network of circadian rhythm
		* KEGG: mmu04710
		* https://doi.org/10.1093/nar/28.1.27
	* M. musculus static network of circadian rhythm
		* KEGG: mmu04713
		* https://doi.org/10.1093/nar/28.1.27

	* M. musculus network of Pi3K/Akt signaling
		* KEGG: mmu04151
		* https://doi.org/10.1093/nar/28.1.27
	* temporal RNA-sequencing dataset of M. musculus circadian rhythm
		* GEO: GSE171975
		* https://doi.org/10.1371/journal.pbio.3001492