[![Documentation Status](https://readthedocs.org/projects/phasik/badge/)](http://phasik.readthedocs.io/)
[![codecov](https://codecov.io/gl/habermann_lab/phasik/branch/master/graph/badge.svg?token=8FXCNM1GSB)](https://codecov.io/gl/habermann_lab/phasik)
[![PyPI version](https://badge.fury.io/py/phasik.svg)](https://badge.fury.io/py/phasik)
[![Downloads](https://static.pepy.tech/personalized-badge/phasik?period=total&units=international_system&left_color=grey&right_color=green&left_text=Downloads)](https://pepy.tech/project/phasik)

# Phasik 

## What is Phasik?

Phasik is a Python library for identifying temporal phases in complex systems modeled as temporal networks. The library contains classes and functions that can be divided into two main parts:

1. Build, analyse, and visualise temporal networks from time series data.
2. Identify temporal phases, over multiple scales, by clustering the snapshots of the temporal network.  

* [**Source**](https://gitlab.com/habermann_lab/phasik)
* [**Bug reports**](https://gitlab.com/habermann_lab/phasik/-/issues)
* [**Documentation**](https://phasik.readthedocs.io/en/latest/)
* [**Tutorials**](https://phasik.readthedocs.io/en/latest/tutorial/index.html)
* [**Notebooks**](https://gitlab.com/habermann_lab/phasik/-/tree/master/notebooks)


## Install Phasik 

Phasik runs on Python 3.7 or higher.  
Install the latest version of `phasik` with `pip`:

```sh
pip install phasik
```

To install this package locally:
* Clone this repository
* Navigate to the folder on your local machine
* Run the following command:
```sh
pip install -e .["all"]
```
* If that command does not work, you may try the following instead
````zsh
pip install -e .\[all\]
````

## Getting Started
To get started, take a look at the [tutorials](https://phasik.readthedocs.io/en/latest/tutorial/index.html) illustrating the library's basic functionality.


## How to Contribute

If you want to contribute to this project, please make sure to read the
[contributing guidelines](CONTRIBUTING.md). We expect respectful and kind interactions by all contributors and users as laid out in our [code of conduct](CODE_OF_CONDUCT.md).

The Phasik community always welcomes contributions, no matter how small. We're happy to help troubleshoot Phasik issues you run into, assist you if you would like to add functionality or fixes to the codebase, or answer any questions you may have.

Some concrete ways that you can get involved:

* **Spread the word** when you use Phasik by sharing with your colleagues and friends.
* **Request a new feature or report a bug** by raising a [new issue](https://gitlab.com/habermann_lab/phasik/-/issues/new).
* **Create a Pull Request (PR)** to address an [open issue](https://gitlab.com/habermann_lab/phasik/-/issues) or add a feature.


## How to Cite
We acknowledge the importance of good software to support research, and we note
that research becomes more valuable when it is communicated effectively. To
demonstrate the value of Phasik, we ask that you cite Phasik in your work.
Currently, the best way to cite Phasik is to go cite the paper where it was first used:  
"[Inferring cell cycle phases from a partially temporal network of protein interactions](https://doi.org/10.1016/j.crmeth.2023.100397)"  
Lucas, M., Morris, A., Townsend-Teague, A., Tichit, L., Habermann, B. H., & Barrat, A.  
*Cell Reports Methods*, **3**(2), 2023  

In addition to the package, this repository contains the notebooks necessary to reproduce the analysis of the paper.  
Version of the library associated to the paper: [![DOI](https://zenodo.org/badge/DOI/10.5281/zenodo.7378779.svg)](https://doi.org/10.5281/zenodo.7378779)


## Developers 

- Maxime Lucas (lead)

- Arthur Morris
- Matteo Neri
- Simone Poetto
- Laurent Tichit
- Alex Townsend-Teague

## Contact

maxime.lucas.work[at]gmail[dot]com


## License
Released under the GNU GENERAL PUBLIC LICENSE v3 (see [`LICENSE`](LICENSE))


## Other Resources
This library may not meet your needs and if this is this case, consider checking out these other resources:
* [Raphtory](https://github.com/Pometry/Raphtory): A package, written in Rust, with a Python interface, for representing, analyzing, and visualizing temporal and static graphs and hypergraphs.
* [Reticula](https://docs.reticula.network/): A package with a Python wrapper of C++ functions for representing, analyzing, and visualizing temporal and static graphs and hypergraphs.
* [Tacoma](https://github.com/benmaier/tacoma): A package in Python for representing, analyzing, and visualizing temporal networks.
* [Teneto](https://github.com/wiheto/teneto): A package in Python for representing, analyzing, and visualizing temporal networks.

